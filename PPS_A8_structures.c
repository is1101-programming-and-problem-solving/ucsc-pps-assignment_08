// SALU Dissanayake - 202029
// IS1101 Programming and Problem Solving
// Assignment 08
// Write down a program to store the details of students using the knowledge of loops, arrays and structures you gained so far.
// First name of the student, subject and its marks for each student needs to be recorded.
// Data should be fetched through user input (do not hard code) and print the data back.
// Program should record and display information of minimum 5 students.

#include <stdio.h>

struct student
{
    char name[20];
    char subject[20];
    int marks;
};

int main()
{
    struct student array[5];

    for (int i = 0; i < 5; i++)
    {
        printf("Enter details of student %d\n", i + 1);

        printf("Enter name: ");
        scanf("%s", array[i].name);

        printf("Enter subject: ");
        scanf("%s", array[i].subject);

        printf("Enter marks: ");
        scanf("%d", &array[i].marks);

        printf("\n");
    }

    printf("Name\t\tSubject\t\tMarks\n");
    printf("-------------------------------------\n");
    for (int i = 0; i < 5; i++)
    {
        printf("%s\t\t%s\t\t%d\n", array[i].name, array[i].subject, array[i].marks);
    }

    return 0;
}
