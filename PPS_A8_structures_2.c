#include <stdio.h>

// Structure definition
struct student
{
    char name[20];
    char subject[20];
    int marks;
};

int main()
{
    // No of students
    int nStd;
    printf("Enter no of students: ");
    scanf("%d", &nStd);

    // Validation
    while (nStd < 5)
    {
        printf("Invalid input !!!\n");
        printf("Enter no of students: ");
        scanf("%d", &nStd);
    }

    printf("\n");

    // Structure variable
    struct student array[nStd];

    // Input
    for (int i = 0; i < nStd; i++)
    {
        printf("Enter details of student %d\n", i + 1);

        printf("Enter name: ");
        scanf("%s", array[i].name);

        printf("Enter subject: ");
        scanf("%s", array[i].subject);

        printf("Enter marks: ");
        scanf("%d", &array[i].marks);

        printf("\n");
    }

    // Output
    printf("Name\t\tSubject\t\tMarks\n");
    printf("-------------------------------------\n");
    for (int i = 0; i < nStd; i++)
    {
        printf("%s\t\t%s\t\t%d\n", array[i].name, array[i].subject, array[i].marks);
    }

    return 0;
}
