# UCSC PPS Assignment_08

**Structures in C**

Write down a program to store the details of students using the knowledge of loops, arrays and structures you gained so far. First name of the student, subject and its marks for each student needs to be recorded. Data should be fetched through user input (do not hard code) and print the data back. Program should record and display information of minimum 5 students.<br>
Hint: Use array to hold structures and use loops to access structures in the array.

[_Initial code_](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_08/-/blob/master/PPS_A8_structures.c)
<br>
[_Updated code_](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_08/-/blob/master/PPS_A8_structures_2.c)
